package main

import (
	"fmt"
)

type grid struct {
	row        int
	column     int
	value      uint8
	candidates []uint8
	key        int
}

func prettyPrint() {
	for _, value := range g {
		switch value.column {
		case 3, 6:
			fmt.Print(value.value, " | ")
		case 9:
			fmt.Println(value.value)
			switch value.row {
			case 3, 6:
				fmt.Println("---------------------")
			}
		default:
			fmt.Print(value.value, " ")
		}
	}
}

func row(r int) []grid {
	var ret []grid
	for _, value := range g {
		if value.row == r {
			ret = append(ret, value)
		}
	}
	return ret
}

func column(c int) []grid {
	var ret []grid
	for _, value := range g {
		if value.column == c {
			ret = append(ret, value)
		}
	}
	return ret
}

func miniGrid(r int, c int) []grid {
	var ret []grid

	switch {
	case r <= 3:
		for _, value := range g {
			switch {
			case c <= 3:
				if value.row <= 3 && value.column <= 3 {
					ret = append(ret, value)
				}
			case c <= 6:
				if value.row <= 3 && value.column > 3 && value.column <= 6 {
					ret = append(ret, value)
				}
			default:
				if value.row <= 3 && value.column > 6 {
					ret = append(ret, value)
				}
			}
		}
	case r <= 6:
		for _, value := range g {
			switch {
			case c <= 3:
				if value.row > 3 && value.row <= 6 && value.column <= 3 {
					ret = append(ret, value)
				}
			case c <= 6:
				if value.row > 3 && value.row <= 6 && value.column > 3 && value.column <= 6 {
					ret = append(ret, value)
				}
			default:
				if value.row > 3 && value.row <= 6 && value.column > 6 {
					ret = append(ret, value)
				}
			}
		}
	default:
		for _, value := range g {
			switch {
			case c <= 3:
				if value.row > 6 && value.column <= 3 {
					ret = append(ret, value)
				}
			case c <= 6:
				if value.row > 6 && value.column > 3 && value.column <= 6 {
					ret = append(ret, value)
				}
			default:
				if value.row > 6 && value.column > 6 {
					ret = append(ret, value)
				}
			}
		}
	}
	return ret

}

func sum() int {
	ret := 0

	for _, value := range g {
		ret += int(value.value)
	}
	return ret
}

var g = [81]grid{}

var allowed = [9]uint8{1, 2, 3, 4, 5, 6, 7, 8, 9}

func main() {
	// create a 9x9
	c := 0
	for row := 1; row <= 9; row++ {
		for column := 1; column <= 9; column++ {
			g[c].column = column
			g[c].row = row
			g[c].key = c
			c++
		}
	}

	sudoku := [][]uint8{
		{0, 3, 0, 7, 0, 9, 0, 0, 0},
		{7, 0, 0, 4, 0, 0, 3, 0, 6},
		{0, 0, 9, 0, 8, 0, 5, 7, 0},
		{0, 0, 0, 0, 1, 0, 4, 0, 5},
		{1, 0, 3, 0, 4, 0, 8, 0, 7},
		{2, 0, 8, 0, 5, 0, 0, 0, 0},
		{0, 9, 7, 0, 3, 0, 2, 0, 0},
		{8, 0, 2, 0, 0, 4, 0, 0, 1},
		{0, 0, 0, 2, 0, 8, 0, 5, 0},
	}

	// load values of sudoku into grid
	for rowKey, rowValue := range sudoku {
		for columnKey, columnValue := range rowValue {
			for key := range g {
				if g[key].row == rowKey+1 && g[key].column == columnKey+1 {
					g[key].value = columnValue
				}
			}
		}
	}

	prettyPrint()
	fmt.Print("\nSolving sudoku\n\n")

	for sum() < 405 {
		solve(1, 1)
		solve(1, 4)
		solve(1, 7)

		solve(4, 1)
		solve(4, 4)
		solve(4, 7)

		solve(7, 1)
		solve(7, 4)
		solve(7, 7)

	}

	prettyPrint()
}

func solve(r int, c int) {
	findCandidates()

	mg := miniGrid(r, c)
	m := make(map[uint8][]grid)
	for _, value := range mg {
		for _, v := range value.candidates {
			m[v] = append(m[v], value)
		}
	}
	for key, value := range m {
		if len(value) == 1 {
			g[value[0].key].value = key
			g[value[0].key].candidates = nil
			//fmt.Println("found", key)

		}
	}
}

func findCandidates() {
	for key, value := range g {
		var candidates []uint8
		mg := miniGrid(value.row, value.column)
		r := row(value.row)
		c := column(value.column)
		unique := make(map[uint8]bool)

		for _, v := range mg {
			unique[v.value] = true
		}

		for _, v := range r {
			unique[v.value] = true
		}

		for _, v := range c {
			unique[v.value] = true
		}

		delete(unique, 0)

		for _, value := range allowed {
			_, ok := unique[value]
			if ok == false {
				candidates = append(candidates, value)
			}

		}

		if g[key].value == 0 {
			g[key].candidates = candidates
		}
	}
}
